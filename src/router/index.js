import { createRouter, createWebHistory } from 'vue-router';
//import Home from '../views/Home.vue'

// ***************** Admin ************************** 
import Admin from '../pages/admin/Admin.vue';
import AdminUserList from '../pages/admin/AdminUserList.vue';
import AdminProductList from '../pages/admin/AdminProductList.vue';
import UserDetail from '../pages/users/userDetail.vue';
import ContactAdmin from '../pages/requests/ContactAdmin.vue';
import RequestsReceived from '../pages/requests/RequestsReceived.vue';
import LoginPage from '../pages/auth/AdminLogin.vue';

// ***************** User ************************** 
import Shop from '../pages/users/Shop.vue';
import User from '../pages/users/User.vue';
import UserLoginPage from '../pages/auth/UserLogin.vue';

import NotFound from '../pages/NotFound.vue';
import store from '../store/index.js';

const router = createRouter({
  history: createWebHistory(),
  routes:[ 
      // admin routes 
      { path: '/', redirect: '/shop' },
      { path: '/bambus', component: LoginPage },
      { path: '/admin', component: Admin,
        beforeEnter(to, from, next){
        if (store.getters["adminAuth/isAuthenticated"]){
           console.log("/adim")
           next()
         }else{
           console.log("go to admin")
          next('/bambus');
         }
        },
      },
      { path: '/admin/productlist', component: AdminProductList, 
      
        beforeEnter(to, from, next){
        if (store.getters["adminAuth/isAuthenticated"]){
           console.log("/productlist")
           next()
         }else{
           console.log("go to admin")
          next('/bambus');
         }
        }, 
      },
      { path: '/admin/userlist', component: AdminUserList, 
        beforeEnter(to, from, next){
        if (store.getters["adminAuth/isAuthenticated"]){
           console.log("userlist")
           next()
         }else{
           console.log("go to admin")
          next('/bambus');
         }
        },
      
      },
      // user routes
      { path: '/login', component: UserLoginPage,
      
        beforeEnter(to, from, next){
        if (store.getters["userAuth/isAuthenticated"] === false){
           next()
         }else{
           next('/shop')
          }
        }


      },
      { path: '/shop', component: Shop},
      { path: '/user', component: User, 
        beforeEnter(to, from, next){
        if (store.getters["userAuth/isAuthenticated"]){
           console.log("/shop")
           next()
         }else{
           console.log("go to admin")
          next('/login');
         }
        },
      },

      // fallback routes
      { path: '/requests', component: RequestsReceived, meta: { requiresAuth: true }},
      { path: '/:notFound(.*)', component: NotFound },
  ],
})
/* 
router.beforeEach(function(to, from, next){
  console.log(to.meta.requiresAuth)
  console.log(store.getters["adminAuth/isAuthenticated"])

  /*
 if(to.meta.requiresAuth && !store.getters["adminAuth/isAuthenticated"]){
   next('/login');
 }else if (to.meta.requiresUnauth && store.getters["adminAuth/isAuthenticated"]){
   console.log("/productlist")
   next('/product')
 }else{
   next();
 }
 */

/*
 if(to.meta.requiresAuth && !store.getters["adminAuth/isAuthenticated"]){
   next('/login');
 } else{
   next();
 }
});

*/
export default router
