import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index.js'

import PrimeVue from 'primevue/config'
import Button from 'primevue/button'
import InputText from 'primevue/inputtext';
import MegaMenu from 'primevue/megamenu';
import Menu from 'primevue/menu';
import Menubar from 'primevue/menubar';
import Card from 'primevue/card';
import Divider from 'primevue/divider';
import Tag from 'primevue/tag';
import Avatar from 'primevue/avatar';
import AvatarGroup from 'primevue/avatargroup';
import Checkbox from 'primevue/checkbox';
import FileUpload from 'primevue/fileupload';
import Toolbar from 'primevue/toolbar';
import Rating from 'primevue/rating';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';
import ProgressSpinner from 'primevue/progressspinner';
// ---------------------------------------------------
// ***************** MyComponents  *******************
// ---------------------------------------------------
import BaseCard from './components/ui/BaseCard.vue';
import BaseButton from './components/ui/BaseButton.vue';
import BaseBadge from './components/ui/BaseBadge.vue';
import Textarea from 'primevue/textarea';
import InputNumber from 'primevue/inputnumber';
import RadioButton from 'primevue/radiobutton'
import Dropdown from 'primevue/dropdown';
import Dialog from 'primevue/dialog';
import Password from 'primevue/password';
import ToggleButton from 'primevue/togglebutton';
import Chips from 'primevue/chips';
import SelectButton from 'primevue/selectbutton';
import ToastService from 'primevue/toastservice';
import Toast from 'primevue/toast';
import TabView from 'primevue/tabview';
import TabPanel from 'primevue/tabpanel';
import ScrollPanel from 'primevue/scrollpanel';
//import CoachFilter from './components/ui/CoachFilter.vue'
import Accordion from 'primevue/accordion';
import AccordionTab from 'primevue/accordiontab';

import 'primevue/resources/themes/saga-blue/theme.css'       //theme
import 'primevue/resources/primevue.min.css'                 //core css
import 'primeicons/primeicons.css'                           //icons
import 'primeflex/primeflex.css'

import './assets/scss/main.scss'

// Vue.config.devtools = true

const app = createApp(App);


// config
app.config.devtools = true

app.use(PrimeVue);
app.use(store);
app.use(router);
app.use(ToastService);
app.component('InputText', InputText);
app.component('Button', Button);
app.component('MegaMenu', MegaMenu);
app.component('Menu', Menu);
app.component('Menubar', Menubar);
app.component('Card', Card);
app.component('Divider', Divider);
app.component('Tag', Tag);
app.component('BaseCard', BaseCard);
app.component('BaseButton', BaseButton);
app.component('BaseBadge',BaseBadge);
app.component('AvatarGroup', AvatarGroup);
app.component('Avatar', Avatar);
app.component('FileUpload', FileUpload);
app.component('Toolbar', Toolbar);
app.component('Rating', Rating);
app.component('DataTable', DataTable);
app.component('Column', Column);
app.component('ColumnGroup', ColumnGroup);
app.component('Textarea', Textarea);
app.component('InputNumber', InputNumber);
app.component('RadioButton', RadioButton);
app.component('Dropdown', Dropdown);
app.component('Dialog', Dialog);
app.component('Password', Password);
app.component('ToggleButton', ToggleButton);
app.component('Chips', Chips);
app.component('SelectButton', SelectButton);
app.component('ProgressSpinner', ProgressSpinner);
app.component('Toast', Toast);
app.component('TabView', TabView);
app.component('TabPanel', TabPanel);
app.component('ScrollPanel', ScrollPanel);
app.component('AccordionTab', AccordionTab);
app.component('Accordion', Accordion);

//app.component('CoachFilter',CoachFilter);
app.component('Checkbox', Checkbox);
app.mount('#app');
