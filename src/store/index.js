import { createStore } from 'vuex'

import productsModule from './modules/products/index.js'
import requestsModule from './modules/requests/index.js'
import adminAuthModule from './modules/adminAuth/index.js'
import usersAuthModule from './modules/userAuth/index.js'

const store = createStore({
  modules:{
    products: productsModule,
    requests: requestsModule,
    userAuth: usersAuthModule,
    adminAuth: adminAuthModule
  },
  state(){
    return{
      userI: "c3"
    }
  },
  getters:{
  }
});

export default store;
