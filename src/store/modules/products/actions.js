export default {
  async loadProducts(context){

    //const token = context.rootGetters["adminAuth/token"]

    const response = await fetch('http://127.0.0.1:8000/products',  {
      mode: 'cors',
      method: 'GET',
      /*
      headers: {
        'Authorization': 'Bearer ' + token
      },
      */
    });

    // response Object
    const products = await response.json();

    // Error messages
    if(!response.ok){
      const error = new Error(responseData.detail || 'Failed to load products')
      return error
    }


    // store user token in Vuex store
    context.commit('addProducts', products )

    console.log("--------------- Store users Vuex ----------")
    console.log(products)
    
    return products;
    // Succes messages
    //return { "message": "Loading of products was successfull "}

  },
  async addProduct(context, data){
    console.log(data)
    const productData = {
      tname : "products",
      titel: data.titel,
      price: data.price,
      description: data.description,
      liter : data.liter,
      deposit : data.deposit,
      side_dish: data.side_dish,
      variety: data.variety,
      ingredients: data.ingredients,
      category: data.category,
      kind: data.kind,
      extra: data.extra,
      images: data.images
    }

    console.log("--------- User Prdoucts Actions Vux --------")

    // console.log(userData)
    const token = context.rootGetters["adminAuth/token"]
    //context.commit('registerCoach', coachData);
    
    const response = await fetch('http://127.0.0.1:8000/v1/post',  {
      mode: 'cors',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify(productData)
    }); 

    const responseData = await response.json();

    if(!response.ok){
       // error
      console.log(response)
    }

    consle.log(response.ok)
    context.commit('addProduct',{
      ...prodcutData,
    })

  },
  async updateProduct(context, data){
    console.log(data.image)
    const productData = {
      id : data.id,
      tname : "products",
      titel: data.titel,
      price: data.price,
      description: data.description,
      liter : data.liter,
      deposit : data.deposit,
      side_dish: data.side_dish,
      variety: data.variety,
      ingredients: data.ingredients,
      category: data.category,
      kind: data.image,
      extra: data.extra,
      images: data.images
    }

    console.log("--------- User update Prdoucts Actions Vux --------")

    // console.log(userData)
    const token = context.rootGetters["adminAuth/token"]
    //context.commit('registerCoach', coachData);
    
    const response = await fetch('http://127.0.0.1:8000/v1/update',  {
      mode: 'cors',
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify(productData)
    }); 


    if(!response.ok){
       // error
      console.log(response)
    }


  },
  async deleteProduct(context, product){
    const productData = {
      id : product.id,
      images: product.images,
      tname : "products",
    }

    console.log("--------- delete Prdoucts Actions Vux --------")

    // console.log(userData)
    const token = context.rootGetters["adminAuth/token"]
    //context.commit('registerCoach', coachData);
    
    const response = await fetch('http://127.0.0.1:8000/v1/delete',  {
      mode: 'cors',
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify(productData)
    }); 

    const responseData = await response.json();

    if(!response.ok){
       // error
      console.log(response)
    }

    return response

  }
};
