export default{
    userId(state){
      return state.userId;
    },
    isAuthenticated(state){
      return !!state.userToken
    },
    user(state){
      return state.user
    },

}
