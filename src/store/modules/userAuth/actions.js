import router from "@/router"

export default {
  async login(context, payload) {

    // store user token in Vuex store
    return context.dispatch('auth', {
      ...payload,
      mode: 'login'
    });

  },
  async auth(context, payload) {
    console.log("--------------Auth")
    console.log(payload)
    let formData = new FormData();
    formData.append('username', payload.username);
    formData.append('password', payload.password);

    const response = await fetch('http://127.0.0.1:8000/token', {
      method: 'POST',
      body: formData
    });

    // response Object
    const responseData = await response.json();

    // Error messages
    if (!response.ok) {
      const error = new Error(responseData.detail || 'Failed to authenticate')
      return error
    }

    localStorage.setItem('userToken', responseData.access_token);
    //localStorage.setItem('userId', responseData.idToken);

    // store user token in Vuex store
    context.commit('setToken', {
      token: responseData.access_token
      // userId: responseData.id,
      // tokenExpiration: responseData.expiration
    });


    // Succes messages
    return { "message": "Login war Erfolgreich" }

  },
  async loadUsersData(context) {

    const token = context.rootGetters["userAuth/userToken"]

    const response = await fetch('http://127.0.0.1:8000/v1/user', {
      mode: 'cors',
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + token
      },
    });

    // response Object
    const user = await response.json();

    // Error messages
    if (!response.ok) {
      const error = new Error(responseData.detail || 'Failed to load products')
      return error
    }


    // store user obct  in Vuex store
    context.commit('addUserData', user)

    console.log("--------------- Store users Vuex ----------")
    console.log(user)

    // Succes messages
    //return { "message": "Loading of products was successfull "}

  },
  async update(context, data) {
    const userData = { 
        tname:"users",
        ...data,
    }
    console.log("in update ", userData)
    const token = context.rootGetters["userAuth/userToken"]

    const response = await fetch('http://127.0.0.1:8000/v1/update',  {
      mode: 'cors',
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token,
        "lowkey": userData.password
      },
      body: JSON.stringify(userData)
    }); 

    // response Object
    const user = await response.json();

    // Error messages
    if (!response.ok) {
      const error = new Error(responseData.detail || 'Failed to load products')
      console.log(error)
      return error
    }


    return user
    // store user obct  in Vuex store
    console.log("--------------- Store update users Vuex ----------")

    // Succes messages
    //return { "message": "Loading of products was successfull "}

  },
  async deleteUser(context, userId){
    const productData = {
      id : userId,
      tname : "users",
    }

    console.log("--------- delete Prdoucts Actions Vux --------")

    // console.log(userData)
    const token = context.rootGetters["userAuth/userToken"]
    //context.commit('registerCoach', coachData);
    
    const response = await fetch('http://127.0.0.1:8000/v1/delete',  {
      mode: 'cors',
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token
      },
      body: JSON.stringify(productData)
    }); 

    const responseData = await response.json();

    if(!response.ok){
       // error
      console.log(response)
    }

    return response

  },
  autoLogin(context) {
    const token = localStorage.getItem('userToken')
    //const userId = localStorage.getItem('userId')
    context.commit('setToken', {
      token: token,
      //user: userId,
      //tokenExpiration: null
    })
  },
  logout(context) {
    context.commit('setToken', {
      token: null
      // userId: responseData.id,
      // tokenExpiration: responseData.expiration
    });
    localStorage.removeItem('userToken');
    router.replace('/shop')

  }
}

